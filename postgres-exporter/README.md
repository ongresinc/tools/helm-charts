# Helm chart to deploy postgres-exporter

## Add repository

```
helm repo add ongres https://ongresinc.gitlab.io/tools/helm-charts
```

# Deployment structure

```mermaid
graph LR
    postgres-exporter-deployment-.-> postgres-exporter    
    postgres-exporter-deployment-.-> Secrets
    postgres-exporter-deployment-.-> Service
    postgres-exporter-deployment-.-> PodDisruptionBudget
    postgres-exporter-deployment-.-> configmap(ConfigMap: <br>- queries.yaml)

    Secrets-.-> configs(Credentials)
    

```

## Installation

```
helm install -n postgres-exporter postgres-exporter \
ongres/postgres-exporter
```
