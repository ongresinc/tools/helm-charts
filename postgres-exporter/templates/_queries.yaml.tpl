{{- define "queries.yaml" }}
pg_replication:
  query: |
    select
    case
      when pg_last_wal_receive_lsn() = pg_last_wal_replay_lsn() then 0
      else extract (EPOCH FROM now() - pg_last_xact_replay_timestamp())::integer
    end as lag,
    case
      when pg_is_in_recovery() then 1
      else 0
    end as is_replica;
  master: true
  metrics:
    - lag:
        usage: "GAUGE"
        description: "Replication lag behind master in seconds"
    - is_replica:
        usage: "GAUGE"
        description: "Indicates if this host is a replica"

pg_postmaster:
  query: "SELECT pg_postmaster_start_time as start_time_seconds from pg_postmaster_start_time()"
  master: true
  metrics:
    - start_time_seconds:
        usage: "GAUGE"
        description: "Time at which postmaster started"

pg_stat_user_tables:
  query: "SELECT current_database() datname, schemaname, relname, seq_scan, seq_tup_read, idx_scan, idx_tup_fetch, n_tup_ins, n_tup_upd, n_tup_del, n_tup_hot_upd, n_live_tup, n_dead_tup, n_mod_since_analyze, COALESCE(last_vacuum, '1970-01-01Z') as last_vacuum, COALESCE(last_autovacuum, '1970-01-01Z') as last_autovacuum, COALESCE(last_analyze, '1970-01-01Z') as last_analyze, COALESCE(last_autoanalyze, '1970-01-01Z') as last_autoanalyze, vacuum_count, autovacuum_count, analyze_count, autoanalyze_count FROM pg_stat_user_tables"
  metrics:
    - datname:
        usage: "LABEL"
        description: "Name of current database"
    - schemaname:
        usage: "LABEL"
        description: "Name of the schema that this table is in"
    - relname:
        usage: "LABEL"
        description: "Name of this table"
    - seq_scan:
        usage: "COUNTER"
        description: "Number of sequential scans initiated on this table"
    - seq_tup_read:
        usage: "COUNTER"
        description: "Number of live rows fetched by sequential scans"
    - idx_scan:
        usage: "COUNTER"
        description: "Number of index scans initiated on this table"
    - idx_tup_fetch:
        usage: "COUNTER"
        description: "Number of live rows fetched by index scans"
    - n_tup_ins:
        usage: "COUNTER"
        description: "Number of rows inserted"
    - n_tup_upd:
        usage: "COUNTER"
        description: "Number of rows updated"
    - n_tup_del:
        usage: "COUNTER"
        description: "Number of rows deleted"
    - n_tup_hot_upd:
        usage: "COUNTER"
        description: "Number of rows HOT updated (i.e., with no separate index update required)"
    - n_live_tup:
        usage: "GAUGE"
        description: "Estimated number of live rows"
    - n_dead_tup:
        usage: "GAUGE"
        description: "Estimated number of dead rows"
    - n_mod_since_analyze:
        usage: "GAUGE"
        description: "Estimated number of rows changed since last analyze"
    - last_vacuum:
        usage: "GAUGE"
        description: "Last time at which this table was manually vacuumed (not counting VACUUM FULL)"
    - last_autovacuum:
        usage: "GAUGE"
        description: "Last time at which this table was vacuumed by the autovacuum daemon"
    - last_analyze:
        usage: "GAUGE"
        description: "Last time at which this table was manually analyzed"
    - last_autoanalyze:
        usage: "GAUGE"
        description: "Last time at which this table was analyzed by the autovacuum daemon"
    - vacuum_count:
        usage: "COUNTER"
        description: "Number of times this table has been manually vacuumed (not counting VACUUM FULL)"
    - autovacuum_count:
        usage: "COUNTER"
        description: "Number of times this table has been vacuumed by the autovacuum daemon"
    - analyze_count:
        usage: "COUNTER"
        description: "Number of times this table has been manually analyzed"
    - autoanalyze_count:
        usage: "COUNTER"
        description: "Number of times this table has been analyzed by the autovacuum daemon"

pg_statio_user_tables:
  query: "SELECT current_database() datname, schemaname, relname, heap_blks_read, heap_blks_hit, idx_blks_read, idx_blks_hit, toast_blks_read, toast_blks_hit, tidx_blks_read, tidx_blks_hit FROM pg_statio_user_tables"
  metrics:
    - datname:
        usage: "LABEL"
        description: "Name of current database"
    - schemaname:
        usage: "LABEL"
        description: "Name of the schema that this table is in"
    - relname:
        usage: "LABEL"
        description: "Name of this table"
    - heap_blks_read:
        usage: "COUNTER"
        description: "Number of disk blocks read from this table"
    - heap_blks_hit:
        usage: "COUNTER"
        description: "Number of buffer hits in this table"
    - idx_blks_read:
        usage: "COUNTER"
        description: "Number of disk blocks read from all indexes on this table"
    - idx_blks_hit:
        usage: "COUNTER"
        description: "Number of buffer hits in all indexes on this table"
    - toast_blks_read:
        usage: "COUNTER"
        description: "Number of disk blocks read from this table's TOAST table (if any)"
    - toast_blks_hit:
        usage: "COUNTER"
        description: "Number of buffer hits in this table's TOAST table (if any)"
    - tidx_blks_read:
        usage: "COUNTER"
        description: "Number of disk blocks read from this table's TOAST table indexes (if any)"
    - tidx_blks_hit:
        usage: "COUNTER"
        description: "Number of buffer hits in this table's TOAST table indexes (if any)"

pg_database:
  query: "SELECT pg_database.datname, pg_database_size(pg_database.datname) as size FROM pg_database"
  master: true
  cache_seconds: 30
  metrics:
    - datname:
        usage: "LABEL"
        description: "Name of the database"
    - size_bytes:
        usage: "GAUGE"
        description: "Disk space used by the database"

pg_stat_user_indexes:
  query: "SELECT current_database() datname, schemaname, relname, indexrelname, idx_scan, idx_tup_read, idx_tup_fetch FROM pg_stat_user_indexes"
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - schemaname:
        usage: "LABEL"
        description: "Name of the schema that this table is in"
    - relname:
        usage: "LABEL"
        description: "Name of the table for this index"
    - indexrelname:
        usage: "LABEL"
        description: "Name of this index"
    - idx_scan:
        usage: "COUNTER"
        description: "Number of index scans initiated on this index"
    - idx_tup_read:
        usage: "COUNTER"
        description: "Number of index entries returned by scans on this index"
    - idx_tup_fetch:
        usage: "COUNTER"
        description: "Number of live table rows fetched by simple index scans using this index"

pg_statio_user_indexes:
  query: "SELECT current_database() datname, schemaname, relname, indexrelname, idx_blks_read, idx_blks_hit FROM pg_statio_user_indexes"
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - schemaname:
        usage: "LABEL"
        description: "Name of the schema that this table is in"
    - relname:
        usage: "LABEL"
        description: "Name of the table for this index"
    - indexrelname:
        usage: "LABEL"
        description: "Name of this index"
    - idx_blks_read:
        usage: "COUNTER"
        description: "Number of disk blocks read from this index"
    - idx_blks_hit:
        usage: "COUNTER"
        description: "Number of buffer hits in this index"

pg_total_relation_size:
  query: |
    SELECT current_database() datname,
           relnamespace::regnamespace as schemaname,
           relname as relname,
           pg_total_relation_size(oid) bytes
      FROM pg_class
     WHERE relkind = 'r';
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - schemaname:
        usage: "LABEL"
        description: "Name of the schema that this table is in"
    - relname:
        usage: "LABEL"
        description: "Name of this table"
    - bytes:
        usage: "GAUGE"
        description: "total disk space usage for the specified table and associated indexes"

pg_blocked:
  query: |
    SELECT
      database.datname,
      count(blocked.transactionid) AS queries,
      '__transaction__' AS table
    FROM pg_catalog.pg_locks blocked
    INNER JOIN pg_catalog.pg_database database
      ON blocked.database = database.oid
    WHERE NOT blocked.granted AND locktype = 'transactionid'
    GROUP BY datname, locktype
    UNION
    SELECT
      database.datname,
      count(blocked.relation) AS queries,
      blocked.relation::regclass::text AS table
    FROM pg_catalog.pg_locks blocked
    INNER JOIN pg_catalog.pg_database database
      ON blocked.database = database.oid
    WHERE NOT blocked.granted AND locktype != 'transactionid'
    GROUP BY datname, relation
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - queries:
        usage: "GAUGE"
        description: "The current number of blocked queries"
    - table:
        usage: "LABEL"
        description: "The table on which a query is blocked"

pg_oldest_blocked:
  query: |
    SELECT datname,
      coalesce(extract('epoch' from max(clock_timestamp() - state_change)), 0) age_seconds
    FROM pg_stat_activity
    WHERE wait_event_type = 'Lock'
    AND state='active'
    GROUP BY datname
  master: true
  metrics:
    - age_seconds:
        usage: "GAUGE"
        description: "Largest number of seconds any transaction is currently waiting on a lock"
    - datname:
        usage: "LABEL"
        description: "Database name"

pg_slow:
  query: |
    SELECT datname, COUNT(*) AS queries
    FROM pg_stat_activity
    WHERE state = 'active' AND (now() - query_start) > '1 seconds'::interval
    GROUP BY datname;
  master: true
  metrics:
    - queries:
        usage: "GAUGE"
        description: "Current number of slow queries"
    - datname:
        usage: "LABEL"
        description: "Database name"

pg_long_running_transactions:
  query: |
    SELECT datname, COUNT(*) as transactions,
    MAX(EXTRACT(EPOCH FROM (clock_timestamp() - xact_start))) AS age_in_seconds
    FROM pg_stat_activity
    WHERE state is distinct from 'idle' AND (now() - xact_start) > '1 minutes'::interval AND query not like '%VACUUM%'
    GROUP BY datname
  master: true
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - queries:
        usage: "GAUGE"
        description: "Current number of long running transactions"
    - age_in_seconds:
        usage: "GAUGE"
        description: "The current maximum transaction age in seconds"

pg_vacuum:
  query: |
    SELECT
      datname,
      COUNT(*) AS queries,
      MAX(EXTRACT(EPOCH FROM (clock_timestamp() - query_start))) AS age_in_seconds
    FROM pg_catalog.pg_stat_activity
    WHERE state = 'active' AND trim(query) ~* '\AVACUUM (?!ANALYZE)'
    GROUP BY datname
  master: true
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - queries:
        usage: "GAUGE"
        description: "The current number of VACUUM queries"
    - age_in_seconds:
        usage: "GAUGE"
        description: "The current maximum VACUUM query age in seconds"

pg_vacuum_analyze:
  query: |
    SELECT
      datname,
      COUNT(*) AS queries,
      MAX(EXTRACT(EPOCH FROM (clock_timestamp() - query_start))) AS age_in_seconds
    FROM pg_catalog.pg_stat_activity
    WHERE state = 'active' AND trim(query) ~* '\AVACUUM ANALYZE'
    GROUP BY datname
  master: true
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - queries:
        usage: "GAUGE"
        description: "The current number of VACUUM ANALYZE queries"
    - age_in_seconds:
        usage: "GAUGE"
        description: "The current maximum VACUUM ANALYZE query age in seconds"

pg_stuck_idle_in_transaction:
  query: |
    SELECT datname,
      COUNT(*) AS queries
    FROM pg_stat_activity
    WHERE state = 'idle in transaction' AND (now() - query_start) > '10 minutes'::interval
    GROUP BY datname
  master: true
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database name"
    - queries:
        usage: "GAUGE"
        description: "Current number of queries that are stuck being idle in transactions"

pg_txid:
  master: true
  query: |
    SELECT
      CASE WHEN pg_is_in_recovery() THEN 'NaN'::float ELSE txid_current() % (2^52)::bigint END AS current,
      CASE WHEN pg_is_in_recovery() THEN 'NaN'::float ELSE txid_snapshot_xmin(txid_current_snapshot()) % (2^52)::bigint END AS xmin,
      CASE WHEN pg_is_in_recovery() THEN 'NaN'::float ELSE txid_current() - txid_snapshot_xmin(txid_current_snapshot()) END AS xmin_age
  metrics:
    - current:
        usage: "COUNTER"
        description: "Current 64-bit transaction id of the query used to collect this metric (truncated to low 52 bits)"
    - xmin:
        usage: "COUNTER"
        description: "Oldest transaction id of a transaction still in progress, i.e. not known committed or aborted (truncated to low 52 bits)"
    - xmin_age:
        usage: "GAUGE"
        description: "Age of oldest transaction still not committed or aborted measured in transaction ids"

pg_database_datfrozenxid:
  master: true
  query: "SELECT datname, age(datfrozenxid) AS age FROM pg_database"
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database Name"
    - age:
        usage: "GAUGE"
        description: "Age of the oldest transaction that has not been frozen."

pg_wal_position:
  master: true
  query: |
    SELECT CASE
           WHEN pg_is_in_recovery()
           THEN (pg_last_wal_receive_lsn() - '0/0') % (2^52)::bigint
           ELSE (pg_current_wal_lsn() - '0/0') % (2^52)::bigint
           END AS bytes
  metrics:
    - bytes:
        usage: "COUNTER"
        description: "Postgres LSN (log sequence number) being generated on primary or replayed on replica (truncated to low 52 bits)"


pg_replication_slots:
  master: true
  query: |
    SELECT slot_name, slot_type,
           case when active then 1.0 else 0.0 end AS active,
           age(xmin) AS xmin_age,
           age(catalog_xmin) AS catalog_xmin_age,
           CASE WHEN pg_is_in_recovery() THEN pg_last_wal_receive_lsn() ELSE pg_current_wal_lsn() END - restart_lsn AS restart_lsn_bytes,
           CASE WHEN pg_is_in_recovery() THEN pg_last_wal_receive_lsn() ELSE pg_current_wal_lsn() END - confirmed_flush_lsn AS confirmed_flush_lsn_bytes
      FROM pg_replication_slots
  metrics:
    - slot_name:
        usage: "LABEL"
        description: "Slot Name"
    - slot_type:
        usage: "LABEL"
        description: "Slot Type"
    - active:
        usage: "GAUGE"
        description: "Boolean flag indicating whether this slot has a consumer streaming from it"
    - xmin_age:
        usage: "GAUGE"
        description: "Age of oldest transaction that cannot be vacuumed due to this replica"
    - catalog_xmin_age:
        usage: "GAUGE"
        description: "Age of oldest transaction that cannot be vacuumed from catalogs due to this replica (used by logical replication)"
    - restart_lsn_bytes:
        usage: "GAUGE"
        description: "Amount of data on in xlog that must be this replica may need to complete recovery"
    - confirmed_flush_lsn_bytes:
        usage: "GAUGE"
        description: "Amount of data on in xlog that must be this replica has not yet received"

pg_table_bloat:
  query: |
    -- https://github.com/ioguix/pgsql-bloat-estimation
    SELECT  current_database() datname, schemaname, tablename, bs*tblpages AS real_size,
    (tblpages-est_tblpages)*bs AS extra_size,
    CASE WHEN tblpages - est_tblpages > 0
        THEN 100 * (tblpages - est_tblpages)/tblpages::float
        ELSE 0
    END AS extra_ratio, fillfactor,
    CASE WHEN tblpages - est_tblpages_ff > 0
        THEN (tblpages-est_tblpages_ff)*bs
        ELSE 0
    END AS bloat_size,
    CASE WHEN tblpages - est_tblpages_ff > 0
        THEN 100 * (tblpages - est_tblpages_ff)/tblpages::float
        ELSE 0
    END AS bloat_ratio, is_na
    -- , tpl_hdr_size, tpl_data_size, (pst).free_percent + (pst).dead_tuple_percent AS real_frag -- (DEBUG INFO)
    FROM (
    SELECT ceil( reltuples / ( (bs-page_hdr)/tpl_size ) ) + ceil( toasttuples / 4 ) AS est_tblpages,
        ceil( reltuples / ( (bs-page_hdr)*fillfactor/(tpl_size*100) ) ) + ceil( toasttuples / 4 ) AS est_tblpages_ff,
        tblpages, fillfactor, bs, tblid, schemaname, tablename, heappages, toastpages, is_na
        -- , tpl_hdr_size, tpl_data_size, pgstattuple(tblid) AS pst -- (DEBUG INFO)
    FROM (
        SELECT
        ( 4 + tpl_hdr_size + tpl_data_size + (2*ma)
            - CASE WHEN tpl_hdr_size%ma = 0 THEN ma ELSE tpl_hdr_size%ma END
            - CASE WHEN ceil(tpl_data_size)::int%ma = 0 THEN ma ELSE ceil(tpl_data_size)::int%ma END
        ) AS tpl_size, bs - page_hdr AS size_per_block, (heappages + toastpages) AS tblpages, heappages,
        toastpages, reltuples, toasttuples, bs, page_hdr, tblid, schemaname, tablename, fillfactor, is_na
        -- , tpl_hdr_size, tpl_data_size
        FROM (
        SELECT
            tbl.oid AS tblid, ns.nspname AS schemaname, tbl.relname AS tablename, tbl.reltuples,
            tbl.relpages AS heappages, coalesce(toast.relpages, 0) AS toastpages,
            coalesce(toast.reltuples, 0) AS toasttuples,
            coalesce(substring(
            array_to_string(tbl.reloptions, ' ')
            FROM 'fillfactor=([0-9]+)')::smallint, 100) AS fillfactor,
            current_setting('block_size')::numeric AS bs,
            CASE WHEN version()~'mingw32' OR version()~'64-bit|x86_64|ppc64|ia64|amd64' THEN 8 ELSE 4 END AS ma,
            24 AS page_hdr,
            23 + CASE WHEN MAX(coalesce(s.null_frac,0)) > 0 THEN ( 7 + count(s.attname) ) / 8 ELSE 0::int END
            + CASE WHEN bool_or(att.attname = 'oid' and att.attnum < 0) THEN 4 ELSE 0 END AS tpl_hdr_size,
            sum( (1-coalesce(s.null_frac, 0)) * coalesce(s.avg_width, 0) ) AS tpl_data_size,
            bool_or(att.atttypid = 'pg_catalog.name'::regtype)
            OR sum(CASE WHEN att.attnum > 0 THEN 1 ELSE 0 END) <> count(s.attname) AS is_na
        FROM pg_attribute AS att
            JOIN pg_class AS tbl ON att.attrelid = tbl.oid
            JOIN pg_namespace AS ns ON ns.oid = tbl.relnamespace
            LEFT JOIN pg_stats AS s ON s.schemaname=ns.nspname
            AND s.tablename = tbl.relname AND s.inherited=false AND s.attname=att.attname
            LEFT JOIN pg_class AS toast ON tbl.reltoastrelid = toast.oid
        WHERE NOT att.attisdropped
            AND tbl.relkind in ('r','m')
        GROUP BY 1,2,3,4,5,6,7,8,9,10
        ORDER BY 2,3
        ) AS s
    ) AS s2
    ) AS s3
    where schemaname not in ('information_schema','pg_catalog')
    ORDER BY schemaname, tablename;
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database Name"
    - schemaname:
        usage: "LABEL"
        description: "Schema Name"
    - tablename:
        usage: "LABEL"
        description: "Table Name"
    - real_size:
        usage: "GAUGE"
        description: "Table real size"
    - extra_size:
        usage: "GAUGE"
        description: "Estimated extra size not used/needed in the table. This extra size is composed by the fillfactor, bloat and alignment padding spaces"
    - extra_ratio:
        usage: "GAUGE"
        description: "Estimated ratio of the real size used by extra_size"
    - fillfactor:
        usage: "GAUGE"
        description: "Table fillfactor"
    - bloat_size:
        usage: "GAUGE"
        description: "Estimated size of the bloat without the extra space kept for the fillfactor."
    - bloat_ratio:
        usage: "GAUGE"
        description: "Estimated ratio of the real size used by bloat_size"
    - is_na:
        usage: "GAUGE"
        description: "Estimation not aplicable, If true, do not trust the stats"

pg_index:
  query: |
    -- https://github.com/ioguix/pgsql-bloat-estimation
    SELECT current_database() as datname, nspname AS schema_name, tblname, idxname, bs*(relpages)::bigint AS real_size,
    bs*(relpages-est_pages)::bigint AS extra_size,
    100 * (relpages-est_pages)::float / relpages AS extra_ratio,
    fillfactor,
    CASE WHEN relpages > est_pages_ff
    THEN bs*(relpages-est_pages_ff)
    ELSE 0
    END AS bloat_size,
    100 * (relpages-est_pages_ff)::float / relpages AS bloat_ratio,
    is_na
    FROM (
    SELECT coalesce(1 +
            ceil(reltuples/floor((bs-pageopqdata-pagehdr)/(4+nulldatahdrwidth)::float)), 0
        ) AS est_pages,
        coalesce(1 +
            ceil(reltuples/floor((bs-pageopqdata-pagehdr)*fillfactor/(100*(4+nulldatahdrwidth)::float))), 0
        ) AS est_pages_ff,
        bs, nspname, tblname, idxname, relpages, fillfactor, is_na
    FROM (
        SELECT maxalign, bs, nspname, tblname, idxname, reltuples, relpages, idxoid, fillfactor,
            ( index_tuple_hdr_bm +
                maxalign - CASE -- Add padding to the index tuple header to align on MAXALIGN
                    WHEN index_tuple_hdr_bm%maxalign = 0 THEN maxalign
                    ELSE index_tuple_hdr_bm%maxalign
                END
                + nulldatawidth + maxalign - CASE -- Add padding to the data to align on MAXALIGN
                    WHEN nulldatawidth = 0 THEN 0
                    WHEN nulldatawidth::integer%maxalign = 0 THEN maxalign
                    ELSE nulldatawidth::integer%maxalign
                END
            )::numeric AS nulldatahdrwidth, pagehdr, pageopqdata, is_na
        FROM (
            SELECT n.nspname, i.tblname, i.idxname, i.reltuples, i.relpages,
                i.idxoid, i.fillfactor, current_setting('block_size')::numeric AS bs,
                CASE
                WHEN version() ~ 'mingw32' OR version() ~ '64-bit|x86_64|ppc64|ia64|amd64' THEN 8
                ELSE 4
                END AS maxalign,
                24 AS pagehdr,
                16 AS pageopqdata,
                CASE WHEN max(coalesce(s.null_frac,0)) = 0
                    THEN 2 -- IndexTupleData size
                    ELSE 2 + (( 32 + 8 - 1 ) / 8)
                END AS index_tuple_hdr_bm,
                sum( (1-coalesce(s.null_frac, 0)) * coalesce(s.avg_width, 1024)) AS nulldatawidth,
                max( CASE WHEN i.atttypid = 'pg_catalog.name'::regtype THEN 1 ELSE 0 END ) > 0 AS is_na
            FROM (
                SELECT ct.relname AS tblname, ct.relnamespace, ic.idxname, ic.attpos, ic.indkey, ic.indkey[ic.attpos], ic.reltuples, ic.relpages, ic.tbloid, ic.idxoid, ic.fillfactor,
                    coalesce(a1.attnum, a2.attnum) AS attnum, coalesce(a1.attname, a2.attname) AS attname, coalesce(a1.atttypid, a2.atttypid) AS atttypid,
                    CASE WHEN a1.attnum IS NULL
                    THEN ic.idxname
                    ELSE ct.relname
                    END AS attrelname
                FROM (
                    SELECT idxname, reltuples, relpages, tbloid, idxoid, fillfactor, indkey,
                        pg_catalog.generate_series(1,indnatts) AS attpos
                    FROM (
                        SELECT ci.relname AS idxname, ci.reltuples, ci.relpages, i.indrelid AS tbloid,
                            i.indexrelid AS idxoid,
                            coalesce(substring(
                                array_to_string(ci.reloptions, ' ')
                                from 'fillfactor=([0-9]+)')::smallint, 90) AS fillfactor,
                            i.indnatts,
                            pg_catalog.string_to_array(pg_catalog.textin(
                                pg_catalog.int2vectorout(i.indkey)),' ')::int[] AS indkey
                        FROM pg_catalog.pg_index i
                        JOIN pg_catalog.pg_class ci ON ci.oid = i.indexrelid
                        WHERE ci.relam=(SELECT oid FROM pg_am WHERE amname = 'btree')
                        AND ci.relpages > 0
                    ) AS idx_data
                ) AS ic
                JOIN pg_catalog.pg_class ct ON ct.oid = ic.tbloid
                LEFT JOIN pg_catalog.pg_attribute a1 ON
                    ic.indkey[ic.attpos] <> 0
                    AND a1.attrelid = ic.tbloid
                    AND a1.attnum = ic.indkey[ic.attpos]
                LEFT JOIN pg_catalog.pg_attribute a2 ON
                    ic.indkey[ic.attpos] = 0
                    AND a2.attrelid = ic.idxoid
                    AND a2.attnum = ic.attpos
            ) i
            JOIN pg_catalog.pg_namespace n ON n.oid = i.relnamespace
            JOIN pg_catalog.pg_stats s ON s.schemaname = n.nspname
                                        AND s.tablename = i.attrelname
                                        AND s.attname = i.attname
            GROUP BY 1,2,3,4,5,6,7,8,9,10,11
        ) AS rows_data_stats
    ) AS rows_hdr_pdg_stats
    ) AS relation_stats
    WHERE nspname != 'pg_catalog'
    ORDER BY nspname, tblname, idxname;
  metrics:
    - datname:
        usage: "LABEL"
        description: "Database Name"
    - schema_name:
        usage: "LABEL"
        description: "Schema Name"
    - tblname:
        usage: "LABEL"
        description: "Table Name"
    - idxname:
        usage: "LABEL"
        description: "Index Name"
    - real_size:
        usage: "GAUGE"
        description: "Index size"
    - extra_size:
        usage: "GAUGE"
        description: "Index extra size"
    - extra_ratio:
        usage: "GAUGE"
        description: "Index extra size ratio"
    - fillfactor:
        usage: "GAUGE"
        description: "Fillfactor"
    - bloat_size:
        usage: "GAUGE"
        description: "Estimate index bloat size"
    - bloat_ratio:
        usage: "GAUGE"
        description: "Estimate index bloat size ratio"
    - is_na:
        usage: "GAUGE"
        description: "Estimate Not aplicable, bad statistic"

pg_replication_status:
  master: true
  query: |
    select application_name,
    client_addr,
    state,
    pg_wal_lsn_diff(pg_stat_replication.sent_lsn, pg_stat_replication.replay_lsn) AS lag_size
    FROM pg_stat_replication;
  metrics:
    - application_name:
        usage: "LABEL"
        description: "Application or node name"
    - client_addr:
        usage: "LABEL"
        description: "Client ip address"
    - state:
        usage: "LABEL"
        description: "Client replication state"
    - lag_size_bytes:
        usage: "GAUGE"
        description: "Replication lag size in bytes"
{{- end }}
