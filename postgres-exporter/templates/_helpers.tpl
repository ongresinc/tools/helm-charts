{{- define "helpers.labels" -}}
application: postgres_exporter
chart-version: {{ .Chart.Version }}
managed-by: {{ .Release.Service }}
cluster: {{ .Values.service.cluster }}
{{- end }}