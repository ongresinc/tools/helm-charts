{{- define "userlist.txt" }}
{{- range $key, $value := .Values.pgbouncer.users }}
{{ $key | quote }} {{ $value.md5_password | quote }}
{{- end }}
{{- end }}