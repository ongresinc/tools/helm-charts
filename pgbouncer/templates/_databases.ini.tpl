{{- define "databases.ini" }}
[databases]
{{- range $key, $value := .Values.databases }}
{{ $key }} = host={{ $value.host }} port={{ $value.port }} {{ if $value.user }}user={{ $value.user }}{{ end }} {{- if $value.password }}password={{ $value.password }}{{ end }} {{ if $value.auth_user }}auth_user={{ $value.auth_user }}{{ end }} dbname={{ $value.dbname }} {{ if $value.pool_size }}pool_size={{ $value.pool_size }}{{ end }}
{{- end }}
{{- end }}