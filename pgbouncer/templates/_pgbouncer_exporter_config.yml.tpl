{{- define "pgbouncer_exporter_config.yml" }}
# The host on which the exporter should listen to (defaults to 127.0.0.1)
exporter_host: {{ if .Values.metrics_exporter.exporter_host }}{{ .Values.metrics_exporter.exporter_host }}{{- else }}0.0.0.0{{ end }}

# The port on which the exporter should listen to (defaults to 9100)
exporter_port: 9100

# The list of pgbouncer instances to monitor
pgbouncers:
  - dsn: postgresql://{{ .Values.metrics_exporter.user }}:{{ .Values.metrics_exporter.password }}@localhost:6432/pgbouncer
    connect_timeout: 5
    {{- if .Values.metrics_exclude_databases }}
    exclude_databases:
    {{- range .Values.metrics_exclude_databases }}
    - {{ . }}
    {{- end }}
    {{- end }}
    {{- if .Values.metrics_exporter.extra_labels }}
    extra_labels:
      {{- range $key, $value := .Values.metrics_exporter.extra_labels }}
      {{ $key }}: {{ $value }}
      {{- end }}
   {{- end }}
{{- end }}
