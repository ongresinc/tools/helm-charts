# Helm chart to deploy PgBouncer

## Add repository

```
helm repo add ongres https://ongresinc.gitlab.io/tools/helm-charts
```

# Deployment structure

```mermaid
graph LR
    pgbouncer-deployment-.-> pgbouncer
    pgbouncer-deployment-.-> postgres-util
    pgbouncer-deployment-.-> pgbouncer-exporter
    pgbouncer-deployment-.-> Secrets
    pgbouncer-deployment-.-> Service
    pgbouncer-deployment-.-> PodDisruptionBudget
    pgbouncer-deployment-.-> configmap(ConfigMap: <br>- pgbouncer.ini)

    Secrets-.-> configs(Configs: <br> - databases.ini <br> - userlist.txt <br> - exporter-config)
    Secrets-.-> Users

    subgraph PgBouncer Pod
    pgbouncer(pgbouncer: <br> - Port: 6432 )
    postgres-util(postgres-util: <br>- Access <br> admin console)
    pgbouncer-exporter(pgbouncer-exporter: <br> - pgbouncer metrics <br> - Port: 9100)
    end
```


## Create a custom values file

Create a value file `pgbouncer_custom_values.yaml` with at least the next params:

```
# pgbouncer configuration and users for userlist.txt file
pgbouncer:
  configuration:
    listen_addr: '*'
    pool_mode: transaction
    default_pool_size: 50
    max_client_conn: 1000
    auth_query: SELECT usename, passwd FROM pg_shadow WHERE usename=$1
    auth_type: md5
    stats_users: pgbouncer

# databases
databases:
  gitlab:
    host: your_db_host
    port: 5432
    dbname: your_db_name
    auth_user: pgbouncer

# service
service:
  type: ClusterIP
  port: 5432

# pgBouncer exporter configuration
metrics_exporter:
  enable: true
  extra_labels:
    cluster: "mycluster"
```

>Note: To check all availables parameters check the chart values.yaml file

## Installation

```
PGBOUNCER_USER=pgbouncer \
PGBOUNCER_PASSWORD=pgbouncer \
MD5_PASSWORD=md5$(echo -n "${PGBOUNCER_PASSWORD}${PGBOUNCER_USER}" | md5sum | awk '{print $1}') \
helm install -n pgbouncer pgbouncer \
-f ./pgbouncer_custom_values.yaml \
--set-string pgbouncer.users.[0].${PGBOUNCER_USER}.md5_password=${MD5_PASSWORD} \
--set-string pgbouncer.users.[0].${PGBOUNCER_USER}.password=${PGBOUNCER_PASSWORD} \
--set-string metrics_exporter.user=${PGBOUNCER_USER} \
--set-string metrics_exporter.password=${PGBOUNCER_PASSWORD} \
ongres/pgbouncer
```

>Note: replace `md5sum` to `md5` if your using Mac


### Create secret for adding extra files

```
apiVersion: v1
kind: Secret
metadata:
  name: pgbouncer-certifcates
  namespace: sg-prod
type: Opaque
data:
  server.crt: |
      #ADD the encrypted string  (cat server.crt | base64 -w0  )
  another.file: |
      #ADD the encrypted string  (cat another.file | base64 -w0  )
```

Then add to the values the keys from the secret:

```
extra_files:
  secret_name: pgbouncer-certificates
  keys:
    - server.crt
    - another.file
```