# Helm chart to deploy Envoy

## Add repository

```bash
helm repo add ongres https://ongresinc.gitlab.io/tools/helm-charts
```

## Configuration

Create a configmap with envoy configuration: 

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: envoy-config
data:
  envoy.yaml: |
    admin:
      address:
        socket_address: { address: 127.0.0.1, port_value: 9901 }

    static_resources:
      listeners:
      - name: listener_0
        address:
          socket_address: { address: 127.0.0.1, port_value: 10000 }
        filter_chains:
    ...
```

## Installing the chart

```bash
helm install --set envoyConfigName=envoy-config envoy ongres/envoy
```